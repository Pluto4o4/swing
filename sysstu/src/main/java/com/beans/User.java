package com.beans;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @program: sysstu
 * @description: 用户类
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 08:41
 **/
public class User {
    @ExcelProperty("用户名")
    private String id;
    @ExcelProperty("密码")
    private String psw;

    public User(String id, String psw) {
        this.id = id;
        this.psw = psw;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", psw='" + psw + '\'' +
                '}';
    }
}
