package com.beans;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @program: sysstu
 * @description: 班级类
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-23 00:23
 **/
public class Class2 {
    @ExcelProperty("id")
    private String classid;
    @ExcelProperty("班级名")
    private String classname;

    public Class2(String classid, String classname) {
        this.classid = classid;
        this.classname = classname;
    }

    public Class2() {
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Override
    public String toString() {
        return "Class{" +
                "classid='" + classid + '\'' +
                ", classname='" + classname + '\'' +
                '}';
    }
}
