package com.beans;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @program: sysstu
 * @description: 学生类
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 11:32
 **/
public class Student {

    @ExcelProperty("学号")
    private String sno;
    @ExcelProperty("名字")
    private String sname;
    @ExcelProperty("性别")
    private String gender;
    @ExcelProperty("班级")
    private String classid;
    @ExcelProperty("民族")
    private String minzu;
    @ExcelProperty("电话")
    private String phone;
    @ExcelProperty("出生日期")
    private String birthDay;
    @ExcelProperty("地址")
    private String address;
    @ExcelProperty("爱好")
    private String interest;
    @ExcelProperty("简历")
    private String jianli;
    @ExcelProperty("照片")
    private String photopath;

    public Student() {
    }

    public Student(String sno, String sname, String gender, String classid, String minzu, String phone, String birthDay, String address, String interest, String jianli, String photopath) {
        this.sno = sno;
        this.sname = sname;
        this.gender = gender;
        this.classid = classid;
        this.minzu = minzu;
        this.phone = phone;
        this.birthDay = birthDay;
        this.address = address;
        this.interest = interest;
        this.jianli = jianli;
        this.photopath = photopath;
    }


    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getClassid() {
        return classid;
    }

    public void setClassid(String classid) {
        this.classid = classid;
    }

    public String getMinzu() {
        return minzu;
    }

    public void setMinzu(String minzu) {
        this.minzu = minzu;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInterest() {
        return interest;
    }

    public void setInterest(String interest) {
        this.interest = interest;
    }

    public String getPhotopath() {
        return photopath;
    }

    public void setPhotopath(String photopath) {
        this.photopath = photopath;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sno='" + sno + '\'' +
                ", sname='" + sname + '\'' +
                ", gender='" + gender + '\'' +
                ", classid='" + classid + '\'' +
                ", minzu='" + minzu + '\'' +
                ", phone='" + phone + '\'' +
                ", birthDay='" + birthDay + '\'' +
                ", address='" + address + '\'' +
                ", interest='" + interest + '\'' +
                ", photopath='" + photopath + '\'' +
                '}';
    }

    public String getJianli() {
        return jianli;
    }

    public void setJianli(String jianli) {
        this.jianli = jianli;
    }
}
