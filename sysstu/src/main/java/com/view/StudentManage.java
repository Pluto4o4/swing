/*
 * Created by JFormDesigner on Fri Oct 21 14:17:41 CST 2022
 */

package com.view;

import java.awt.*;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.*;

import com.Utility.ExcelUtils;
import com.dao.RemoveStudentDao;
import com.dao.StudentDao;
import com.dao.StudentDaoOrder;
import net.miginfocom.swing.*;

/**
 * @author 1
 */
public class StudentManage extends JFrame {
    public StudentManage() {
        initComponents();
    }

    public String getOrder(){
       String a= (String)this.comboBox1.getSelectedItem();
       String res="";
       switch(a){
          case "学号":
             res="sno";
             break;
          case "班级":
             res="classid";
             break;
          case "生日":
             res="birthday";
             break;
          case "民族":
             res="minzu";
             break;
          case "姓名":
             res="sname";
             break;
          case "电话号码":
             res="phone";
             break;
          case "地址":
             res="address";
             break;
       }
       return res;
    }
    public String getClassId(){
       return (String)this.comboBox2.getSelectedItem();
    }

    private void SelectAllMouseClicked(MouseEvent e) {
        // TODO add your code here

       StudentDaoOrder std=new StudentDaoOrder();
       String order=getOrder();
//       order="phone";
       String classId=getClassId();
       ResultSet rs=std.getallclass(classId,order);
       int row=std.getallrows(classId);

        StudentDao std1=new StudentDao();
        if(classId.equals("所有")){
           rs=std1.getallclass(order);
           row=std1.getallrows();
        }

        if(rs!=null) {
            try {
                rs.next();
                Object[][] objs = new Object[row][11];
                System.out.println(rs.getRow());
                for(int i=0;i<row;i++){
                    objs[i][0]=rs.getString("sno");
                    objs[i][1]=rs.getString("sname");
                    objs[i][2]=rs.getString("gender");
                    objs[i][3]=rs.getString("classid");
                    objs[i][4]=rs.getString("minzu");
                    objs[i][5]=rs.getString("phone");
                    objs[i][6]=rs.getString("birthday");
                    objs[i][7]=rs.getString("address");
                    objs[i][8]=rs.getString("interest");
                    objs[i][9]=rs.getString("jianli");
                    objs[i][10]=rs.getString("photopath");
                    rs.next();
                }
                Object[] heads={"学号","姓名","性别","班级编号","民族","电话","出生年月","地址","兴趣爱好","简历","头像路径"};
                DefaultTableModel dtm=(DefaultTableModel) this.table1.getModel();
//               Arrays.sort(objs,(a,b)->a[5].toString().compareTo(b[5].toString()));
                dtm.setDataVector(objs,heads);
                this.table1.repaint();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }else{
            JOptionPane.showMessageDialog(null,"无数据");
        }
    }
    private void AddStudentMouseClicked(MouseEvent e) {
        // TODO add your code here
        new AddStudentFrame(this).setVisible(true);
    }

    private void RemoveMouseClicked(MouseEvent e) {
        // TODO add your code here
        int num=this.table1.getSelectedRow();
        if(num<0){
            JOptionPane.showMessageDialog(this,"没选中任何数据");
        }
        String classid=this.table1.getValueAt(num,0).toString();
//        RemoveStudentDao rem=new RemoveStudentDao();
//        rem.removesudent(classid);
        new RemoveStudentFrame(this,classid).setVisible(true);
//更新数据表格
        StudentDao std=new StudentDao();
        ResultSet rs=std.getallclass();
        int row=std.getallrows();
        if(rs!=null) {
            try {
                rs.next();
                Object[][] objs = new Object[row][11];
                System.out.println(rs.getRow());
                for(int i=0;i<row;i++){
                    objs[i][0]=rs.getString("sno");
                    objs[i][1]=rs.getString("sname");
                    objs[i][2]=rs.getString("gender");
                    objs[i][3]=rs.getString("classid");
                    objs[i][4]=rs.getString("minzu");
                    objs[i][5]=rs.getString("phone");
                    objs[i][6]=rs.getString("birthday");
                    objs[i][7]=rs.getString("address");
                    objs[i][8]=rs.getString("interest");
                    objs[i][9]=rs.getString("jianli");
                    objs[i][10]=rs.getString("photopath");
                    rs.next();
                }
                Object[] heads={"学号","姓名","性别","班级编号","民族","电话","出生年月","地址","兴趣爱好","简历","头像路径"};
                DefaultTableModel dtm=(DefaultTableModel) this.table1.getModel();
                dtm.setDataVector(objs,heads);
                this.table1.repaint();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }else{
            JOptionPane.showMessageDialog(null,"无数据");
        }
    }

    private void update(){
        StudentDao std=new StudentDao();
        ResultSet rs=std.getallclass();
        int row=std.getallrows();
        if(rs!=null) {
            try {
                rs.next();
                Object[][] objs = new Object[row][11];
                System.out.println(rs.getRow());
                for(int i=0;i<row;i++){
                    objs[i][0]=rs.getString("sno");
                    objs[i][1]=rs.getString("sname");
                    objs[i][2]=rs.getString("gender");
                    objs[i][3]=rs.getString("classid");
                    objs[i][4]=rs.getString("minzu");
                    objs[i][5]=rs.getString("phone");
                    objs[i][6]=rs.getString("birthday");
                    objs[i][7]=rs.getString("address");
                    objs[i][8]=rs.getString("interest");
                    objs[i][9]=rs.getString("jianli");
                    objs[i][10]=rs.getString("photopath");
                    rs.next();
                }
                Object[] heads={"学号","姓名","性别","班级编号","民族","电话","出生年月","地址","兴趣爱好","简历","头像路径"};
                DefaultTableModel dtm=(DefaultTableModel) this.table1.getModel();
                dtm.setDataVector(objs,heads);
                this.table1.repaint();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }else{
            JOptionPane.showMessageDialog(null,"无数据");
        }
    }
    private void ChangeMouseClicked(MouseEvent e) {
        // TODO add your code here
        int[] row=this.table1.getSelectedRows();
        if(row.length>2){
            JOptionPane.showMessageDialog(null,"选择过多行!");
        }else if(row.length==0){
            JOptionPane.showMessageDialog(null,"为选中!");
        }else{
            try {
                String id=this.table1.getValueAt(row[0],0).toString();
                ChangeStudentFrame chg=new ChangeStudentFrame(this,id);
                chg.setVisible(true);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private void Excel2MouseClicked(MouseEvent e) {
        // TODO add your code here
        new ExcelUtils().simpleWrite();
        JOptionPane.showMessageDialog(this,"导出成功");
    }

    private void thisWindowActivated(WindowEvent e) {
        update();
        // TODO add your code here
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        panel1 = new JPanel();
        label1 = new JLabel();
        comboBox1 = new JComboBox<>();
        label2 = new JLabel();
        comboBox2 = new JComboBox<>();
        button1 = new JButton();
        button9 = new JButton();
        button7 = new JButton();
        button8 = new JButton();
        button2 = new JButton();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();

        //======== this ========
        setTitle("\u5b66\u751f\u4fe1\u606f\u7ba1\u7406");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowActivated(WindowEvent e) {
                thisWindowActivated(e);
            }
        });
        Container contentPane = getContentPane();
        contentPane.setLayout(new MigLayout(
            "hidemode 3,alignx center",
            // columns
            "[fill]" +
            "[fill]" +
            "[fill]",
            // rows
            "[]" +
            "[]"));

        //======== panel1 ========
        {
            panel1.setLayout(new MigLayout(
                "hidemode 3,align center center",
                // columns
                "[fill]" +
                "[fill]" +
                "[fill]" +
                "[fill]" +
                "[fill]" +
                "[fill]" +
                "[fill]" +
                "[fill]",
                // rows
                "[]"));

            //---- label1 ----
            label1.setText("\u6309\uff1a");
            panel1.add(label1, "cell 0 0");

            //---- comboBox1 ----
            comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
                "\u5b66\u53f7",
                "\u73ed\u7ea7",
                "\u751f\u65e5",
                "\u6c11\u65cf",
                "\u59d3\u540d",
                "\u7535\u8bdd\u53f7\u7801 ",
                "\u5730\u5740"
            }));
            panel1.add(comboBox1, "cell 1 0");

            //---- label2 ----
            label2.setText("\u73ed\u7ea7\uff1a");
            panel1.add(label2, "cell 2 0");

            //---- comboBox2 ----
            comboBox2.setModel(new DefaultComboBoxModel<>(new String[] {
                "\u6240\u6709",
                "1\u73ed",
                "2\u73ed",
                "3\u73ed",
                "4\u73ed",
                "5\u73ed"
            }));
            panel1.add(comboBox2, "cell 3 0");

            //---- button1 ----
            button1.setText("\u67e5\u8be2");
            button1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    SelectAllMouseClicked(e);
                }
            });
            panel1.add(button1, "cell 4 0");

            //---- button9 ----
            button9.setText("\u6dfb\u52a0");
            button9.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    AddStudentMouseClicked(e);
                }
            });
            panel1.add(button9, "cell 5 0");

            //---- button7 ----
            button7.setText("\u4fee\u6539");
            button7.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    ChangeMouseClicked(e);
                }
            });
            panel1.add(button7, "cell 5 0");

            //---- button8 ----
            button8.setText("\u5220\u9664");
            button8.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    RemoveMouseClicked(e);
                }
            });
            panel1.add(button8, "cell 6 0");

            //---- button2 ----
            button2.setText("\u5bfc\u51faexcel");
            button2.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    Excel2MouseClicked(e);
                }
            });
            panel1.add(button2, "cell 7 0");
        }
        contentPane.add(panel1, "cell 1 0");

        //======== scrollPane1 ========
        {

            //---- table1 ----
            table1.setModel(new DefaultTableModel(
                new Object[][] {
                },
                new String[] {
                    "\u5b66\u53f7", "\u59d3\u540d", "\u6027\u522b", "\u73ed\u7ea7\u7f16\u53f7", "\u540d\u65cf", "\u7535\u8bdd", "\u51fa\u751f\u5e74\u6708", "\u5730\u5740", "\u5174\u8da3\u7231\u597d", "\u7b80\u5386", "\u5934\u50cf\u8def\u5f84"
                }
            ));
            scrollPane1.setViewportView(table1);
        }
        contentPane.add(scrollPane1, "cell 1 1,width 300:100%,height 300:90%");
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel panel1;
    private JLabel label1;
    private JComboBox<String> comboBox1;
    private JLabel label2;
    private JComboBox<String> comboBox2;
    private JButton button1;
    private JButton button9;
    private JButton button7;
    private JButton button8;
    private JButton button2;
    private JScrollPane scrollPane1;
    private JTable table1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
