/*
 * Created by JFormDesigner on Fri Oct 21 14:17:41 CST 2022
 */

package com.view;

import java.awt.*;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.*;
import javax.swing.table.*;

import com.Utility.ExcelUtils;
import com.Utility.ExcelUtilsclass;
import com.dao.ChangeClassDao;
import com.dao.RemoveClassDao;
import com.dao.RemoveStudentDao;
import com.dao.SelectallClassDao;
import com.dao.StudentDao;
import net.miginfocom.swing.*;

/**
 * @author 1
 */
public class classManage extends JFrame {
    public classManage() {
        initComponents();
    }
    public void updateStudentList(){
        StudentDao std=new StudentDao();
        ResultSet rs=std.getallclass();
        int row=std.getallrows();
        if(rs!=null) {
            try {
                rs.next();
                Object[][] objs = new Object[row][11];
                System.out.println(rs.getRow());
                for(int i=0;i<row;i++){
                    objs[i][0]=rs.getString("sno");
                    objs[i][1]=rs.getString("sname");
                    objs[i][2]=rs.getString("gender");
                    objs[i][3]=rs.getString("classid");
                    objs[i][4]=rs.getString("minzu");
                    objs[i][5]=rs.getString("phone");
                    objs[i][6]=rs.getString("birthday");
                    objs[i][7]=rs.getString("address");
                    objs[i][8]=rs.getString("interest");
                    objs[i][9]=rs.getString("jianli");
                    objs[i][10]=rs.getString("photopath");
                    rs.next();
                }
                Object[] heads={"学号","姓名","性别","班级编号","民族","电话","出生年月","地址","兴趣爱好","简历","头像路径"};
                DefaultTableModel dtm=(DefaultTableModel) this.table1.getModel();
                dtm.setDataVector(objs,heads);
                this.table1.repaint();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }else{
            JOptionPane.showMessageDialog(null,"无数据");
        }

    }

    private void SelectAllMouseClicked(MouseEvent e) {
        updateStudentList();
        // TODO add your code here

    }

    private void AddStudentMouseClicked(MouseEvent e) {
        // TODO add your code here
        new AddStudentFrame(this).setVisible(true);
    }

    private void RemoveMouseClicked(MouseEvent e) {
        // TODO add your code here
        int num=this.table1.getSelectedRow();
        if(num<0){
            JOptionPane.showMessageDialog(this,"没选中任何数据");
        }
        String classid=this.table1.getValueAt(num,0).toString();
        RemoveClassDao rem=new RemoveClassDao();
        rem.removeclass(classid);
////更新数据表格
//        StudentDao std=new StudentDao();
//        ResultSet rs=std.getallclass();
//        int row=std.getallrows();
//        if(rs!=null) {
//            try {
//                rs.next();
//                Object[][] objs = new Object[row][11];
//                System.out.println(rs.getRow());
//                for(int i=0;i<row;i++){
//                    objs[i][0]=rs.getString("sno");
//                    objs[i][1]=rs.getString("sname");
//                    objs[i][2]=rs.getString("gender");
//                    objs[i][3]=rs.getString("classid");
//                    objs[i][4]=rs.getString("minzu");
//                    objs[i][5]=rs.getString("phone");
//                    objs[i][6]=rs.getString("birthday");
//                    objs[i][7]=rs.getString("address");
//                    objs[i][8]=rs.getString("interest");
//                    objs[i][9]=rs.getString("jianli");
//                    objs[i][10]=rs.getString("photopath");
//                    rs.next();
//                }
//                Object[] heads={"学号","姓名","性别","班级编号","民族","电话","出生年月","地址","兴趣爱好","简历","头像路径"};
//                DefaultTableModel dtm=(DefaultTableModel) this.table1.getModel();
//                dtm.setDataVector(objs,heads);
//                this.table1.repaint();
//            } catch (SQLException ex) {
//                throw new RuntimeException(ex);
//            }
//        }else{
//            JOptionPane.showMessageDialog(null,"无数据");
//        }
    }

    private void update(){
        StudentDao std=new StudentDao();
        ResultSet rs=std.getallclass();
        int row=std.getallrows();
        if(rs!=null) {
            try {
                rs.next();
                Object[][] objs = new Object[row][11];
                System.out.println(rs.getRow());
                for(int i=0;i<row;i++){
                    objs[i][0]=rs.getString("sno");
                    objs[i][1]=rs.getString("sname");
                    objs[i][2]=rs.getString("gender");
                    objs[i][3]=rs.getString("classid");
                    objs[i][4]=rs.getString("minzu");
                    objs[i][5]=rs.getString("phone");
                    objs[i][6]=rs.getString("birthday");
                    objs[i][7]=rs.getString("address");
                    objs[i][8]=rs.getString("interest");
                    objs[i][9]=rs.getString("jianli");
                    objs[i][10]=rs.getString("photopath");
                    rs.next();
                }
                Object[] heads={"学号","姓名","性别","班级编号","民族","电话","出生年月","地址","兴趣爱好","简历","头像路径"};
                DefaultTableModel dtm=(DefaultTableModel) this.table1.getModel();
                dtm.setDataVector(objs,heads);
                this.table1.repaint();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }else{
            JOptionPane.showMessageDialog(null,"无数据");
        }
    }
    private void ChangeMouseClicked(MouseEvent e) {
        // TODO add your code here
        int[] row=this.table1.getSelectedRows();
        if(row.length>2){
            JOptionPane.showMessageDialog(null,"选择过多行!");
        }else if(row.length==0){
            JOptionPane.showMessageDialog(null,"为选中!");
        }else{
            try {
                String id=this.table1.getValueAt(row[0],0).toString();
                ChangeClassFrame chg=new ChangeClassFrame(this,id);
                chg.setVisible(true);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }


    private void thisWindowActivated(WindowEvent e) {
        updateStudentList();
        // TODO add your code here
        SelectallClassDao std=new SelectallClassDao();
        ResultSet rs=std.getallclass();
        int row=std.getallrows();
        if(rs!=null) {
            try {
                rs.next();
                Object[][] objs = new Object[row][2];
                System.out.println(rs.getRow());
                for(int i=0;i<row;i++){
                    objs[i][0]=rs.getString("classid");
                    objs[i][1]=rs.getString("classname");
                    rs.next();
                }
                Object[] heads={"班级id","班级名"};
                DefaultTableModel dtm=(DefaultTableModel) this.table1.getModel();
                dtm.setDataVector(objs,heads);
                this.table1.repaint();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }else{
            JOptionPane.showMessageDialog(null,"无数据");
        }
    }

    private void selectAllMouseClicked(MouseEvent e) {
        // TODO add your code here
        SelectallClassDao std=new SelectallClassDao();
        ResultSet rs=std.getallclass();
        int row=std.getallrows();
        if(rs!=null) {
            try {
                rs.next();
                Object[][] objs = new Object[row][2];
                System.out.println(rs.getRow());
                for(int i=0;i<row;i++){
                    objs[i][0]=rs.getString("classid");
                    objs[i][1]=rs.getString("classname");
                    rs.next();
                }
                Object[] heads={"班级id","班级名"};
                DefaultTableModel dtm=(DefaultTableModel) this.table1.getModel();
                dtm.setDataVector(objs,heads);
                this.table1.repaint();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }else{
            JOptionPane.showMessageDialog(null,"无数据");
        }
    }

    private void addClassMouseClicked(MouseEvent e) {
        // TODO add your code here
        new AddClassFrame(this).setVisible(true);
    }

    private void ExcelMouseClicked(MouseEvent e) {
        // TODO add your code here
        new ExcelUtilsclass().simpleWrite();
        JOptionPane.showMessageDialog(this,"导出成功");
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        panel1 = new JPanel();
        button1 = new JButton();
        button9 = new JButton();
        button7 = new JButton();
        button8 = new JButton();
        button2 = new JButton();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();

        //======== this ========
        setTitle("\u73ed\u7ea7\u4fe1\u606f\u7ba1\u7406");
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowActivated(WindowEvent e) {
                thisWindowActivated(e);
            }
        });
        Container contentPane = getContentPane();
        contentPane.setLayout(new MigLayout(
            "hidemode 3,alignx center",
            // columns
            "[fill]" +
            "[fill]" +
            "[fill]",
            // rows
            "[]" +
            "[]"));

        //======== panel1 ========
        {
            panel1.setLayout(new MigLayout(
                "hidemode 3,align center center",
                // columns
                "[fill]" +
                "[fill]" +
                "[fill]" +
                "[fill]",
                // rows
                "[]"));

            //---- button1 ----
            button1.setText("\u67e5\u8be2\u6240\u6709");
            button1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    selectAllMouseClicked(e);
                }
            });
            panel1.add(button1, "cell 0 0");

            //---- button9 ----
            button9.setText("\u6dfb\u52a0");
            button9.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    addClassMouseClicked(e);
                }
            });
            panel1.add(button9, "cell 1 0");

            //---- button7 ----
            button7.setText("\u4fee\u6539");
            button7.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    ChangeMouseClicked(e);
                }
            });
            panel1.add(button7, "cell 1 0");

            //---- button8 ----
            button8.setText("\u5220\u9664");
            button8.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    RemoveMouseClicked(e);
                }
            });
            panel1.add(button8, "cell 2 0");

            //---- button2 ----
            button2.setText("\u5bfc\u51fa\u4e3aexcel");
            button2.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    ExcelMouseClicked(e);
                }
            });
            panel1.add(button2, "cell 3 0");
        }
        contentPane.add(panel1, "cell 1 0");

        //======== scrollPane1 ========
        {

            //---- table1 ----
            table1.setModel(new DefaultTableModel(
                new Object[][] {
                },
                new String[] {
                    "\u73ed\u7ea7id", "\u73ed\u7ea7\u540d"
                }
            ));
            scrollPane1.setViewportView(table1);
        }
        contentPane.add(scrollPane1, "cell 1 1,width 300:100%,height 300:90%");
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel panel1;
    private JButton button1;
    private JButton button9;
    private JButton button7;
    private JButton button8;
    private JButton button2;
    private JScrollPane scrollPane1;
    private JTable table1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
