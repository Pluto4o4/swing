/*
 * Created by JFormDesigner on Thu Oct 20 17:27:12 CST 2022
 */

package com.view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import com.dao.UserDao;
import net.miginfocom.swing.*;

/**
 * @author 1
 */
public class LoginFrame extends JFrame {
    public LoginFrame() {
        initComponents();
    }

    private void LoginMouseClicked(MouseEvent e) {
        // TODO add your code here
        String username=this.textField1.getText();
        String password=this.passwordField1.getText();
        UserDao login=new UserDao();
        if(login.getuser(username,password)){
            System.out.println("成功");
            this.setVisible(false);
            new MainFrame().setVisible(true);
        }else{
            new LoginFailedFrame(this).setVisible(true);
        }

    }

    private void reMouseClicked(MouseEvent e) {
        // TODO add your code here
        this.textField1.setText("");
        this.passwordField1.setText("");
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        panel1 = new JPanel();
        label1 = new JLabel();
        textField1 = new JTextField();
        panel2 = new JPanel();
        label2 = new JLabel();
        passwordField1 = new JPasswordField();
        panel3 = new JPanel();
        button1 = new JButton();
        button2 = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new MigLayout(
            "hidemode 3,align center center",
            // columns
            "[fill]",
            // rows
            "[]" +
            "[]" +
            "[]"));

        //======== panel1 ========
        {
            panel1.setLayout(new MigLayout(
                "hidemode 3",
                // columns
                "[fill]" +
                "[fill]",
                // rows
                "[]"));

            //---- label1 ----
            label1.setText("\u767b\u5f55");
            panel1.add(label1, "cell 0 0");
            panel1.add(textField1, "cell 1 0,width 90%");
        }
        contentPane.add(panel1, "cell 0 0");

        //======== panel2 ========
        {
            panel2.setLayout(new MigLayout(
                "hidemode 3",
                // columns
                "[fill]" +
                "[fill]",
                // rows
                "[]"));

            //---- label2 ----
            label2.setText("\u8d26\u53f7");
            panel2.add(label2, "cell 0 0");
            panel2.add(passwordField1, "cell 1 0,width 90%");
        }
        contentPane.add(panel2, "cell 0 1");

        //======== panel3 ========
        {
            panel3.setLayout(new MigLayout(
                "hidemode 3,align center center",
                // columns
                "[fill]" +
                "[fill]",
                // rows
                "[]"));

            //---- button1 ----
            button1.setText("\u767b\u5f55");
            button1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    LoginMouseClicked(e);
                }
            });
            panel3.add(button1, "cell 0 0");

            //---- button2 ----
            button2.setText("\u91cd\u7f6e");
            button2.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    reMouseClicked(e);
                }
            });
            panel3.add(button2, "cell 2 0");
        }
        contentPane.add(panel3, "cell 0 2");
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel panel1;
    private JLabel label1;
    private JTextField textField1;
    private JPanel panel2;
    private JLabel label2;
    private JPasswordField passwordField1;
    private JPanel panel3;
    private JButton button1;
    private JButton button2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
