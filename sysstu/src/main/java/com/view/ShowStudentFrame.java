/*
 * Created by JFormDesigner on Fri Oct 21 19:26:21 CST 2022
 */

package com.view;

import com.beans.Student;
import com.dao.ChangeStudentDao;
import com.dao.RemoveStudentDao2;
import com.dao.SelectStudentDao;
import com.view.StudentManage;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author 1
 */
public class ShowStudentFrame extends JDialog {
    private StudentManage frame=null;
    public ShowStudentFrame(Window owner, String id) {
        super(owner);
        initComponents();
        Student stu;

        stu=new SelectStudentDao().outcome(id);
        this.textField1.setText(stu.getSno());
        this.textField1.setEditable(false);
        this.textField2.setText(stu.getSname());
        this.textField2.setEditable(false);
        this.comboBox1.setSelectedItem(stu.getClassid());
        this.comboBox1.setEnabled(false);
        if(stu.getGender()=="女"){
            this.radioButton1.setSelected(true);
        }else{
            this.radioButton2.setSelected(true);
        }
        this.radioButton1.setEnabled(false);
        this.radioButton2.setEnabled(false);
        this.comboBox2.setSelectedItem(stu.getMinzu());
        this.comboBox2.setEnabled(false);
        this.textField3.setText(stu.getPhone());
        this.textField3.setEditable(false);
        this.textField4.setText(stu.getBirthDay());
        this.textField4.setEditable(false);
        this.textField5.setText(stu.getPhotopath());
        this.textField5.setEditable(false);
        String interest=stu.getInterest();
        if(interest.contains("音乐")){
            this.checkBox1.setSelected(true);
        }
        if(interest.contains("游戏")){
            this.checkBox2.setSelected(true);
        }
        if(interest.contains("交朋友")){
            this.checkBox3.setSelected(true);
        }
        if(interest.contains("上网")){
            this.checkBox4.setSelected(true);
        }
        this.checkBox1.setEnabled(false);
        this.checkBox2.setEnabled(false);
        this.checkBox3.setEnabled(false);
        this.checkBox4.setEnabled(false);
        this.textArea1.setText(stu.getJianli());
        this.textArea1.setEditable(false);
        this.textField1.setEditable(false);
    }

    private void changeMouseClicked(MouseEvent e) {
        // TODO add your code here
        ChangeStudentDao chang=new ChangeStudentDao();
        Student stu=new Student();
        String name=this.textField2.getText();
        String Classid=this.comboBox1.getSelectedItem().toString();
        String gendre;
        if(this.radioButton2.isSelected()){
            gendre="女";
        }else{
            gendre="男";
        }
        String mingzu=this.comboBox2.getSelectedItem().toString();
        String phone=this.textField3.getText();
        String hobby="";
        if(this.checkBox1.isSelected()){
            hobby=hobby+"音乐";
        }
        if(this.checkBox2.isSelected()){
            hobby=hobby+",游戏";
        }
        if(this.checkBox3.isSelected()){
            hobby=hobby+",交朋友";
        }
        if(this.checkBox4.isSelected()){
            hobby=hobby+",上网";
        }
        String birthday=this.textField4.getText();
        String jianli=this.textArea1.getText();
        String address=this.textField5.getText();

        stu.setSname(name);
        stu.setGender(gendre);
        stu.setClassid(Classid);
        stu.setMinzu(mingzu);
        stu.setPhone(phone);
        stu.setBirthDay(birthday);
        stu.setAddress(address);
        stu.setInterest(hobby);
        stu.setJianli(jianli);

        int i=chang.change(stu);
        if(i>0){
            JOptionPane.showMessageDialog(null,"修改成功");
            this.setVisible(false);
        }else{
            JOptionPane.showMessageDialog(null,"修改失败");
        }
    }

    private void RemoveMouseClicked(MouseEvent e) {
        // TODO add your code here
        new RemoveStudentDao2().removesudent(this.textField1.getText());
    }

    private void button2MouseClicked(MouseEvent e) {
        // TODO add your code here
        this.setVisible(false);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        panel2 = new JPanel();
        panel19 = new JPanel();
        label11 = new JLabel();
        button3 = new JButton();
        label1 = new JLabel();
        textField1 = new JTextField();
        label2 = new JLabel();
        textField2 = new JTextField();
        label3 = new JLabel();
        comboBox1 = new JComboBox<>();
        label4 = new JLabel();
        radioButton2 = new JRadioButton();
        comboBox2 = new JComboBox<>();
        label5 = new JLabel();
        label6 = new JLabel();
        textField3 = new JTextField();
        label7 = new JLabel();
        textField4 = new JTextField();
        label8 = new JLabel();
        textField5 = new JTextField();
        label9 = new JLabel();
        checkBox1 = new JCheckBox();
        checkBox2 = new JCheckBox();
        checkBox3 = new JCheckBox();
        checkBox4 = new JCheckBox();
        label10 = new JLabel();
        scrollPane1 = new JScrollPane();
        textArea1 = new JTextArea();
        button2 = new JButton();
        radioButton1 = new JRadioButton();
        panel1 = new JPanel();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== panel2 ========
        {
            panel2.setBorder(new TitledBorder("\u5b66\u751f\u57fa\u672c\u4fe1\u606f"));
            panel2.setLayout(null);

            //======== panel19 ========
            {
                panel19.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
                panel19.setLayout(new MigLayout(
                    "hidemode 3,align center center",
                    // columns
                    "[fill]",
                    // rows
                    "[]"));

                //---- label11 ----
                label11.setText("<html><br><br>\u4e2a<br>\u4eba<br>\u7167<br>\u7247<br><br></html>  ");
                panel19.add(label11, "cell 0 0");
            }
            panel2.add(panel19);
            panel19.setBounds(465, 110, 150, panel19.getPreferredSize().height);

            //---- button3 ----
            button3.setText("\u4e0a\u4f20\u7167\u7247");
            panel2.add(button3);
            button3.setBounds(new Rectangle(new Point(500, 270), button3.getPreferredSize()));

            //---- label1 ----
            label1.setText("\u5b66\u53f7\uff1a");
            panel2.add(label1);
            label1.setBounds(new Rectangle(new Point(40, 70), label1.getPreferredSize()));
            panel2.add(textField1);
            textField1.setBounds(80, 65, 120, textField1.getPreferredSize().height);

            //---- label2 ----
            label2.setText("\u59d3\u540d\uff1a");
            panel2.add(label2);
            label2.setBounds(new Rectangle(new Point(40, 110), label2.getPreferredSize()));
            panel2.add(textField2);
            textField2.setBounds(80, 105, 120, textField2.getPreferredSize().height);

            //---- label3 ----
            label3.setText("\u73ed\u7ea7\uff1a");
            panel2.add(label3);
            label3.setBounds(new Rectangle(new Point(250, 110), label3.getPreferredSize()));

            //---- comboBox1 ----
            comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
                "1\u73ed",
                "2\u73ed",
                "3\u73ed",
                "4\u73ed",
                "5\u73ed"
            }));
            panel2.add(comboBox1);
            comboBox1.setBounds(290, 105, 120, comboBox1.getPreferredSize().height);

            //---- label4 ----
            label4.setText("\u6027\u522b\uff1a");
            panel2.add(label4);
            label4.setBounds(40, 150, 37, 20);

            //---- radioButton2 ----
            radioButton2.setText("\u5973");
            radioButton2.setSelected(true);
            panel2.add(radioButton2);
            radioButton2.setBounds(new Rectangle(new Point(140, 150), radioButton2.getPreferredSize()));

            //---- comboBox2 ----
            comboBox2.setModel(new DefaultComboBoxModel<>(new String[] {
                "\u6c49\u65cf",
                "\u5c11\u6570\u6c11\u65cf"
            }));
            panel2.add(comboBox2);
            comboBox2.setBounds(290, 145, 120, comboBox2.getPreferredSize().height);

            //---- label5 ----
            label5.setText("\u6c11\u65cf\uff1a");
            panel2.add(label5);
            label5.setBounds(250, 150, 37, label5.getPreferredSize().height);

            //---- label6 ----
            label6.setText("\u8054\u7cfb\u7535\u8bdd\uff1a");
            panel2.add(label6);
            label6.setBounds(15, 190, 68, label6.getPreferredSize().height);
            panel2.add(textField3);
            textField3.setBounds(80, 185, 120, textField3.getPreferredSize().height);

            //---- label7 ----
            label7.setText("\u51fa\u751f\u65e5\u671f\uff1a");
            panel2.add(label7);
            label7.setBounds(new Rectangle(new Point(225, 190), label7.getPreferredSize()));
            panel2.add(textField4);
            textField4.setBounds(290, 185, 120, textField4.getPreferredSize().height);

            //---- label8 ----
            label8.setText("\u8054\u7cfb\u5730\u5740\uff1a");
            panel2.add(label8);
            label8.setBounds(15, 230, 68, 21);
            panel2.add(textField5);
            textField5.setBounds(80, 225, 330, textField5.getPreferredSize().height);

            //---- label9 ----
            label9.setText("\u4e2a\u4eba\u7231\u597d\uff1a");
            panel2.add(label9);
            label9.setBounds(15, 270, 68, label9.getPreferredSize().height);

            //---- checkBox1 ----
            checkBox1.setText("\u97f3\u4e50");
            panel2.add(checkBox1);
            checkBox1.setBounds(new Rectangle(new Point(100, 270), checkBox1.getPreferredSize()));

            //---- checkBox2 ----
            checkBox2.setText("\u6e38\u620f");
            panel2.add(checkBox2);
            checkBox2.setBounds(new Rectangle(new Point(170, 270), checkBox2.getPreferredSize()));

            //---- checkBox3 ----
            checkBox3.setText("\u4ea4\u670b\u53cb");
            panel2.add(checkBox3);
            checkBox3.setBounds(new Rectangle(new Point(230, 270), checkBox3.getPreferredSize()));

            //---- checkBox4 ----
            checkBox4.setText("\u4e0a\u7f51");
            panel2.add(checkBox4);
            checkBox4.setBounds(new Rectangle(new Point(315, 270), checkBox4.getPreferredSize()));

            //---- label10 ----
            label10.setText("\u4e2a\u4eba\u7b80\u5386\uff1a");
            panel2.add(label10);
            label10.setBounds(15, 310, 68, label10.getPreferredSize().height);

            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(textArea1);
            }
            panel2.add(scrollPane1);
            scrollPane1.setBounds(80, 310, 330, 100);

            //---- button2 ----
            button2.setText("\u8fd4\u56de");
            button2.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    button2MouseClicked(e);
                }
            });
            panel2.add(button2);
            button2.setBounds(new Rectangle(new Point(265, 445), button2.getPreferredSize()));

            //---- radioButton1 ----
            radioButton1.setText("\u7537");
            panel2.add(radioButton1);
            radioButton1.setBounds(new Rectangle(new Point(90, 150), radioButton1.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                    Rectangle bounds = panel2.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(panel2);
        panel2.setBounds(15, 5, 650, 500);

        //======== panel1 ========
        {
            panel1.setLayout(null);

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                    Rectangle bounds = panel1.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(panel1);
        panel1.setBounds(new Rectangle(new Point(725, 292), panel1.getPreferredSize()));

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        pack();
        setLocationRelativeTo(getOwner());

        //---- buttonGroup1 ----
        ButtonGroup buttonGroup1 = new ButtonGroup();
        buttonGroup1.add(radioButton2);
        buttonGroup1.add(radioButton1);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel panel2;
    private JPanel panel19;
    private JLabel label11;
    private JButton button3;
    private JLabel label1;
    private JTextField textField1;
    private JLabel label2;
    private JTextField textField2;
    private JLabel label3;
    private JComboBox<String> comboBox1;
    private JLabel label4;
    private JRadioButton radioButton2;
    private JComboBox<String> comboBox2;
    private JLabel label5;
    private JLabel label6;
    private JTextField textField3;
    private JLabel label7;
    private JTextField textField4;
    private JLabel label8;
    private JTextField textField5;
    private JLabel label9;
    private JCheckBox checkBox1;
    private JCheckBox checkBox2;
    private JCheckBox checkBox3;
    private JCheckBox checkBox4;
    private JLabel label10;
    private JScrollPane scrollPane1;
    private JTextArea textArea1;
    private JButton button2;
    private JRadioButton radioButton1;
    private JPanel panel1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
