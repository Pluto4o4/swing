/*
 * Created by JFormDesigner on Fri Oct 21 19:26:21 CST 2022
 */

package com.view;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.beans.Student;
import com.dao.InsertStudentDao;
import javafx.stage.FileChooser;
import net.miginfocom.swing.*;

/**
 * @author 1
 */
public class AddStudentFrame extends JDialog {
    private String srcPath;
    public AddStudentFrame(Window owner) {
        super(owner);
        initComponents();
//        ImageIcon icon=new ImageIcon("E:\\javafile\\sysstu\\src\\main\\resources\\1.jpg");
//        icon.setImage(icon.getImage().getScaledInstance(this.panel21.getWidth(),this.panel21.getHeight(),Image.SCALE_DEFAULT));
//        label12.setIcon(icon);
//        label12.updateUI();


    }

    private void AddMouseClicked(MouseEvent e) {
        // TODO add your code here
        InsertStudentDao in=new InsertStudentDao();
        Student stu=new Student();
        String id=this.textField1.getText();
        String name=this.textField2.getText();
        String Classid=this.comboBox1.getSelectedItem().toString();
        String gendre;
        if(this.radioButton4.isSelected()){
            gendre="女";
        }else{
            gendre="男";
        }
        String mingzu=this.comboBox2.getSelectedItem().toString();
        String phone=this.textField3.getText();
        String hobby="";
        if(this.checkBox5.isSelected()){
            hobby=hobby+"音乐";
        }
        if(this.checkBox6.isSelected()){
            hobby=hobby+",游戏";
        }
        if(this.checkBox7.isSelected()){
            hobby=hobby+",交朋友";
        }
        if(this.checkBox8.isSelected()){
            hobby=hobby+",上网";
        }
        String birthday=this.textField4.getText();
        String jianli=this.textArea2.getText();
        String address=this.textField5.getText();

        stu.setSno(id);
        stu.setSname(name);
        stu.setGender(gendre);
        stu.setClassid(Classid);
        stu.setMinzu(mingzu);
        stu.setPhone(phone);
        stu.setBirthDay(birthday);
        stu.setAddress(address);
        stu.setInterest(hobby);
        stu.setJianli(jianli);
        stu.setPhotopath(srcPath);

        int i=in.insertStudent(stu);
        if(i>0){
            this.setVisible(false);
        }
    }

    private void UploadMouseClicked(MouseEvent e) {
        // TODO add your code here
//            JFileChooser fg=new JFileChooser();
//            fg.setCurrentDirectory(new File("/"));
//            fg.setMultiSelectionEnabled(false);
//            fg.setFileFilter(new FileNameExtensionFilter("image(*.jpg,*.png,*.gif)","jpg","png","gif"));
//            int result=fg.showOpenDialog(null);
        FileDialog fg=new FileDialog(this);
            fg.setMultipleMode(false);
            fg.setVisible(true);
            String srcDir=fg.getDirectory();
            String srcFileName=fg.getFile();
            String srcPath=srcDir+"\\"+srcFileName;
            String[]  suffixImage={".png",".jpg",".bmp",".gif"};
            boolean isSuffix=false;
            for(int i=0;i<suffixImage.length;i++){
               if(srcFileName.contains(suffixImage[i])) {
                   isSuffix = true;
                   break;
               }
            }
            if(!isSuffix){
                JOptionPane.showMessageDialog(this,"文件类型错误!");
            }
            File srcFile=new File(srcPath);
        if (srcFile.length()>100*1024) {
            JOptionPane.showMessageDialog(this,"图片过大(选择100kb以下的)!");
        }

        uploadFile(srcFile);
        //加载图片
        ImageIcon icons=new ImageIcon(srcPath);
        System.out.println(srcPath);
        icons.setImage(icons.getImage().getScaledInstance(this.label12.getWidth(),this.label12.getHeight(),Image.SCALE_DEFAULT));
        label12.setIcon(icons);
        label12.updateUI();
    }

    public boolean uploadFile(File srcFile){
        boolean result=false;
        String desDir="E:\\javafile\\sysstu\\src\\main\\resources\\image\\";
        String desPath=desDir+"/"+srcFile.getName();
        this.srcPath=desPath;
        if(srcFile==null || !srcFile.exists()){
            JOptionPane.showMessageDialog(this,"路径有误！");
            return false;
        }
        try {
            FileInputStream inputStream=new FileInputStream(srcFile);
            FileOutputStream outputStream=new FileOutputStream(desPath);
            byte[] byteData=new byte[(int)srcFile.length()];
            inputStream.read(byteData);
            outputStream.write(byteData);
            outputStream.flush();
            outputStream.close();
            inputStream.close();
            result=true;
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void label12MouseClicked(MouseEvent e) {
        // TODO add your code here
        this.setVisible(false);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        panel20 = new JPanel();
        panel21 = new JPanel();
        label12 = new JLabel();
        button4 = new JButton();
        label13 = new JLabel();
        textField1 = new JTextField();
        label14 = new JLabel();
        textField2 = new JTextField();
        label15 = new JLabel();
        comboBox1 = new JComboBox<>();
        label16 = new JLabel();
        radioButton4 = new JRadioButton();
        comboBox2 = new JComboBox<>();
        label17 = new JLabel();
        label18 = new JLabel();
        textField3 = new JTextField();
        label19 = new JLabel();
        textField4 = new JTextField();
        label20 = new JLabel();
        textField5 = new JTextField();
        label21 = new JLabel();
        checkBox5 = new JCheckBox();
        checkBox6 = new JCheckBox();
        checkBox7 = new JCheckBox();
        checkBox8 = new JCheckBox();
        label22 = new JLabel();
        scrollPane2 = new JScrollPane();
        textArea2 = new JTextArea();
        button5 = new JButton();
        button6 = new JButton();
        radioButton1 = new JRadioButton();
        panel22 = new JPanel();

        //======== this ========
        setTitle("\u6dfb\u52a0");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== panel20 ========
        {
            panel20.setBorder(new TitledBorder("\u6dfb\u52a0\u5b66\u751f\u57fa\u672c\u4fe1\u606f"));
            panel20.setLayout(null);

            //======== panel21 ========
            {
                panel21.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
                panel21.setLayout(new MigLayout(
                    "hidemode 3,align center center",
                    // columns
                    "[fill]",
                    // rows
                    "[]"));

                //---- label12 ----
                label12.setText("           \u4e2a\u4eba\u7167\u7247");
                label12.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        label12MouseClicked(e);
                    }
                });
                panel21.add(label12, "cell 0 0,align center center,grow 0 0,wmin 130,hmin 120");
            }
            panel20.add(panel21);
            panel21.setBounds(465, 110, 150, panel21.getPreferredSize().height);

            //---- button4 ----
            button4.setText("\u4e0a\u4f20\u7167\u7247");
            button4.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    UploadMouseClicked(e);
                }
            });
            panel20.add(button4);
            button4.setBounds(new Rectangle(new Point(500, 270), button4.getPreferredSize()));

            //---- label13 ----
            label13.setText("\u5b66\u53f7\uff1a");
            panel20.add(label13);
            label13.setBounds(new Rectangle(new Point(40, 70), label13.getPreferredSize()));
            panel20.add(textField1);
            textField1.setBounds(80, 65, 120, textField1.getPreferredSize().height);

            //---- label14 ----
            label14.setText("\u59d3\u540d\uff1a");
            panel20.add(label14);
            label14.setBounds(new Rectangle(new Point(40, 110), label14.getPreferredSize()));
            panel20.add(textField2);
            textField2.setBounds(80, 105, 120, textField2.getPreferredSize().height);

            //---- label15 ----
            label15.setText("\u73ed\u7ea7\uff1a");
            panel20.add(label15);
            label15.setBounds(new Rectangle(new Point(250, 110), label15.getPreferredSize()));

            //---- comboBox1 ----
            comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
                "1\u73ed",
                "2\u73ed",
                "3\u73ed",
                "4\u73ed",
                "5\u73ed"
            }));
            panel20.add(comboBox1);
            comboBox1.setBounds(290, 105, 120, comboBox1.getPreferredSize().height);

            //---- label16 ----
            label16.setText("\u6027\u522b\uff1a");
            panel20.add(label16);
            label16.setBounds(40, 150, 37, 20);

            //---- radioButton4 ----
            radioButton4.setText("\u5973");
            radioButton4.setSelected(true);
            panel20.add(radioButton4);
            radioButton4.setBounds(new Rectangle(new Point(140, 150), radioButton4.getPreferredSize()));

            //---- comboBox2 ----
            comboBox2.setModel(new DefaultComboBoxModel<>(new String[] {
                "\u6c49\u65cf",
                "\u5c11\u6570\u6c11\u65cf"
            }));
            panel20.add(comboBox2);
            comboBox2.setBounds(290, 145, 120, comboBox2.getPreferredSize().height);

            //---- label17 ----
            label17.setText("\u6c11\u65cf\uff1a");
            panel20.add(label17);
            label17.setBounds(250, 150, 37, label17.getPreferredSize().height);

            //---- label18 ----
            label18.setText("\u8054\u7cfb\u7535\u8bdd\uff1a");
            panel20.add(label18);
            label18.setBounds(15, 190, 68, label18.getPreferredSize().height);
            panel20.add(textField3);
            textField3.setBounds(80, 185, 120, textField3.getPreferredSize().height);

            //---- label19 ----
            label19.setText("\u51fa\u751f\u65e5\u671f\uff1a");
            panel20.add(label19);
            label19.setBounds(new Rectangle(new Point(225, 190), label19.getPreferredSize()));
            panel20.add(textField4);
            textField4.setBounds(290, 185, 120, textField4.getPreferredSize().height);

            //---- label20 ----
            label20.setText("\u8054\u7cfb\u5730\u5740\uff1a");
            panel20.add(label20);
            label20.setBounds(15, 230, 68, 21);
            panel20.add(textField5);
            textField5.setBounds(80, 225, 330, textField5.getPreferredSize().height);

            //---- label21 ----
            label21.setText("\u4e2a\u4eba\u7231\u597d\uff1a");
            panel20.add(label21);
            label21.setBounds(15, 270, 68, label21.getPreferredSize().height);

            //---- checkBox5 ----
            checkBox5.setText("\u97f3\u4e50");
            panel20.add(checkBox5);
            checkBox5.setBounds(new Rectangle(new Point(100, 270), checkBox5.getPreferredSize()));

            //---- checkBox6 ----
            checkBox6.setText("\u6e38\u620f");
            panel20.add(checkBox6);
            checkBox6.setBounds(new Rectangle(new Point(170, 270), checkBox6.getPreferredSize()));

            //---- checkBox7 ----
            checkBox7.setText("\u4ea4\u670b\u53cb");
            panel20.add(checkBox7);
            checkBox7.setBounds(new Rectangle(new Point(230, 270), checkBox7.getPreferredSize()));

            //---- checkBox8 ----
            checkBox8.setText("\u4e0a\u7f51");
            panel20.add(checkBox8);
            checkBox8.setBounds(new Rectangle(new Point(315, 270), checkBox8.getPreferredSize()));

            //---- label22 ----
            label22.setText("\u4e2a\u4eba\u7b80\u5386\uff1a");
            panel20.add(label22);
            label22.setBounds(15, 310, 68, label22.getPreferredSize().height);

            //======== scrollPane2 ========
            {
                scrollPane2.setViewportView(textArea2);
            }
            panel20.add(scrollPane2);
            scrollPane2.setBounds(80, 310, 330, 100);

            //---- button5 ----
            button5.setText("\u6dfb\u52a0");
            button5.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    AddMouseClicked(e);
                }
            });
            panel20.add(button5);
            button5.setBounds(190, 445, 79, button5.getPreferredSize().height);

            //---- button6 ----
            button6.setText("\u8fd4\u56de");
            panel20.add(button6);
            button6.setBounds(new Rectangle(new Point(375, 445), button6.getPreferredSize()));

            //---- radioButton1 ----
            radioButton1.setText("\u7537");
            panel20.add(radioButton1);
            radioButton1.setBounds(new Rectangle(new Point(90, 150), radioButton1.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel20.getComponentCount(); i++) {
                    Rectangle bounds = panel20.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel20.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel20.setMinimumSize(preferredSize);
                panel20.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(panel20);
        panel20.setBounds(15, 5, 700, 500);

        //======== panel22 ========
        {
            panel22.setLayout(null);

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel22.getComponentCount(); i++) {
                    Rectangle bounds = panel22.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel22.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel22.setMinimumSize(preferredSize);
                panel22.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(panel22);
        panel22.setBounds(new Rectangle(new Point(725, 292), panel22.getPreferredSize()));

        {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        pack();
        setLocationRelativeTo(getOwner());

        //---- buttonGroup1 ----
        ButtonGroup buttonGroup1 = new ButtonGroup();
        buttonGroup1.add(radioButton4);
        buttonGroup1.add(radioButton1);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel panel20;
    private JPanel panel21;
    private JLabel label12;
    private JButton button4;
    private JLabel label13;
    private JTextField textField1;
    private JLabel label14;
    private JTextField textField2;
    private JLabel label15;
    private JComboBox<String> comboBox1;
    private JLabel label16;
    private JRadioButton radioButton4;
    private JComboBox<String> comboBox2;
    private JLabel label17;
    private JLabel label18;
    private JTextField textField3;
    private JLabel label19;
    private JTextField textField4;
    private JLabel label20;
    private JTextField textField5;
    private JLabel label21;
    private JCheckBox checkBox5;
    private JCheckBox checkBox6;
    private JCheckBox checkBox7;
    private JCheckBox checkBox8;
    private JLabel label22;
    private JScrollPane scrollPane2;
    private JTextArea textArea2;
    private JButton button5;
    private JButton button6;
    private JRadioButton radioButton1;
    private JPanel panel22;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
