/*
 * Created by JFormDesigner on Fri Oct 21 09:47:05 CST 2022
 */

package com.view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import net.miginfocom.swing.*;

/**
 * @author 1
 */
public class MainFrame extends JFrame {
    public MainFrame() {
        initComponents();
    }

    private void menu1(ActionEvent e) {
        // TODO add your code here
    }

    private void menuItem1(ActionEvent e) {
        // TODO add your code here
        if(e.getSource()==this.menuItem1){
            new StudentManage().setVisible(true);
        }
    }


    private void menuItem5(ActionEvent e) {
        // TODO add your code here
        new classManage().setVisible(true);
    }

    private void StudentAddmenuItem2(ActionEvent e) {
        // TODO add your code here
        new AddStudentFrame(this).setVisible(true);
    }


    private void ChangemenuItem3(ActionEvent e) {
        // TODO add your code here
        new SelectStudent().setVisible(true);
    }

    private void RemovemenuItem4(ActionEvent e) {
        // TODO add your code here
        new SelectStudent2().setVisible(true);
    }

    private void menuItem8(ActionEvent e) {
        // TODO add your code here
        new UserManage().setVisible(true);
    }

    private void menuItem6MouseClicked(MouseEvent e) {
        // TODO add your code here
        new SelectStudentForId().setVisible(true);
    }

    private void ShowmenuItem6(ActionEvent e) {
        // TODO add your code here
        new SelectStudentForId().setVisible(true);
    }


    private void menuItem12(ActionEvent e) {
        // TODO add your code here
        System.exit(0);
    }

    private void menuItem7(ActionEvent e) {
        // TODO add your code here
        new StudentManage().setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        menuBar1 = new JMenuBar();
        menu1 = new JMenu();
        menuItem1 = new JMenuItem();
        menuItem5 = new JMenuItem();
        menuItem2 = new JMenuItem();
        menuItem3 = new JMenuItem();
        menuItem4 = new JMenuItem();
        menuItem12 = new JMenuItem();
        menuItem11 = new JMenuItem();
        menu2 = new JMenu();
        menuItem6 = new JMenuItem();
        menu3 = new JMenu();
        menuItem7 = new JMenuItem();
        menu4 = new JMenu();
        menuItem8 = new JMenuItem();
        menuItem9 = new JMenuItem();
        menuItem10 = new JMenuItem();
        scrollPane1 = new JScrollPane();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new MigLayout(
            "hidemode 3,align center center",
            // columns
            "[fill]" +
            "[fill]",
            // rows
            "[]" +
            "[]"));

        //======== menuBar1 ========
        {

            //======== menu1 ========
            {
                menu1.setText("\u6570\u636e\u7ef4\u62a4");
                menu1.setIcon(new ImageIcon(getClass().getResource("/toolbar/borrowbook (\u81ea\u5b9a\u4e49) (\u81ea\u5b9a\u4e49).png")));

                //---- menuItem1 ----
                menuItem1.setText("\u5b66\u751f\u7ba1\u7406");
                menuItem1.setIcon(new ImageIcon(getClass().getResource("/toolbar/readerinfo (\u81ea\u5b9a\u4e49).png")));
                menuItem1.addActionListener(e -> menuItem1(e));
                menu1.add(menuItem1);

                //---- menuItem5 ----
                menuItem5.setText("\u73ed\u7ea7\u7ba1\u7406");
                menuItem5.setIcon(new ImageIcon(getClass().getResource("/toolbar/library (\u81ea\u5b9a\u4e49).png")));
                menuItem5.addActionListener(e -> menuItem5(e));
                menu1.add(menuItem5);

                //---- menuItem2 ----
                menuItem2.setText("\u5b66\u751f\u6dfb\u52a0");
                menuItem2.setIcon(new ImageIcon(getClass().getResource("/toolbar/exit (\u81ea\u5b9a\u4e49).png")));
                menuItem2.addActionListener(e -> StudentAddmenuItem2(e));
                menu1.add(menuItem2);

                //---- menuItem3 ----
                menuItem3.setText("\u5b66\u751f\u4fee\u6539");
                menuItem3.setIcon(new ImageIcon(getClass().getResource("/toolbar/register (\u81ea\u5b9a\u4e49).png")));
                menuItem3.addActionListener(e -> ChangemenuItem3(e));
                menu1.add(menuItem3);

                //---- menuItem4 ----
                menuItem4.setText("\u5b66\u751f\u5220\u9664");
                menuItem4.setIcon(new ImageIcon(getClass().getResource("/toolbar/storage (\u81ea\u5b9a\u4e49).png")));
                menuItem4.addActionListener(e -> RemovemenuItem4(e));
                menu1.add(menuItem4);

                //---- menuItem12 ----
                menuItem12.setText("\u9000\u51fa\u7cfb\u7edf");
                menuItem12.setIcon(new ImageIcon(getClass().getResource("/toolbar/cancel (\u81ea\u5b9a\u4e49).png")));
                menuItem12.addActionListener(e -> menuItem12(e));
                menu1.add(menuItem12);
                menu1.add(menuItem11);
            }
            menuBar1.add(menu1);

            //======== menu2 ========
            {
                menu2.setText("\u6570\u636e\u67e5\u8be2");
                menu2.setIcon(new ImageIcon(getClass().getResource("/toolbar/tt1 (\u81ea\u5b9a\u4e49).png")));

                //---- menuItem6 ----
                menuItem6.setText("\u6309\u5b66\u53f7\u67e5\u8be2");
                menuItem6.setIcon(new ImageIcon(getClass().getResource("/toolbar/query (\u81ea\u5b9a\u4e49).png")));
                menuItem6.addActionListener(e -> ShowmenuItem6(e));
                menu2.add(menuItem6);
            }
            menuBar1.add(menu2);

            //======== menu3 ========
            {
                menu3.setText("\u6570\u636e\u663e\u793a");
                menu3.setIcon(new ImageIcon(getClass().getResource("/toolbar/print (\u81ea\u5b9a\u4e49).png")));

                //---- menuItem7 ----
                menuItem7.setText("\u6d4f\u89c8");
                menuItem7.setIcon(new ImageIcon(getClass().getResource("/toolbar/modifybook (\u81ea\u5b9a\u4e49).png")));
                menuItem7.addActionListener(e -> menuItem7(e));
                menu3.add(menuItem7);
            }
            menuBar1.add(menu3);

            //======== menu4 ========
            {
                menu4.setText("\u7cfb\u7edf\u7ef4\u62a4");
                menu4.setIcon(new ImageIcon(getClass().getResource("/toolbar/modifyreader (\u81ea\u5b9a\u4e49).png")));

                //---- menuItem8 ----
                menuItem8.setText("\u7528\u6237\u7ba1\u7406");
                menuItem8.setIcon(new ImageIcon(getClass().getResource("/toolbar/tt1 (\u81ea\u5b9a\u4e49).png")));
                menuItem8.addActionListener(e -> menuItem8(e));
                menu4.add(menuItem8);

                //---- menuItem9 ----
                menuItem9.setText("\u5173\u4e8e");
                menuItem9.setIcon(new ImageIcon(getClass().getResource("/toolbar/tt9 (\u81ea\u5b9a\u4e49).png")));
                menu4.add(menuItem9);

                //---- menuItem10 ----
                menuItem10.setText("\u5e2e\u52a9");
                menuItem10.setIcon(new ImageIcon(getClass().getResource("/toolbar/switch (\u81ea\u5b9a\u4e49).png")));
                menu4.add(menuItem10);
            }
            menuBar1.add(menu4);
        }
        setJMenuBar(menuBar1);
        contentPane.add(scrollPane1, "cell 0 0,width 300:100%,height 300:100%");
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JMenuBar menuBar1;
    private JMenu menu1;
    private JMenuItem menuItem1;
    private JMenuItem menuItem5;
    private JMenuItem menuItem2;
    private JMenuItem menuItem3;
    private JMenuItem menuItem4;
    private JMenuItem menuItem12;
    private JMenuItem menuItem11;
    private JMenu menu2;
    private JMenuItem menuItem6;
    private JMenu menu3;
    private JMenuItem menuItem7;
    private JMenu menu4;
    private JMenuItem menuItem8;
    private JMenuItem menuItem9;
    private JMenuItem menuItem10;
    private JScrollPane scrollPane1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
