/*
 * Created by JFormDesigner on Sat Oct 22 23:31:23 CST 2022
 */

package com.view;

import com.view.AddStudentFrame;
import com.view.ChangeStudentFrame;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author 1
 */
public class SelectStudentForId extends JFrame {
    public SelectStudentForId() {
        initComponents();
    }

    private void okButtonMouseClicked(MouseEvent e) {
        // TODO add your code here
        this.setVisible(true);
        new AddStudentFrame(null).setVisible(true);
    }

    private void selectMouseClicked(MouseEvent e) {
        // TODO add your code here
        this.setVisible(false);
        new ShowStudentFrame(this,this.textField1.getText()).setVisible(true);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        panel3 = new JPanel();
        label2 = new JLabel();
        textField1 = new JTextField();
        button1 = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new MigLayout(
                    "insets 0,hidemode 3,align center center",
                    // columns
                    "[fill]" +
                    "[fill]" +
                    "[fill]",
                    // rows
                    "[fill]" +
                    "[]" +
                    "[]"));

                //======== panel3 ========
                {
                    panel3.setLayout(new MigLayout(
                        "hidemode 3",
                        // columns
                        "[fill]" +
                        "[fill]",
                        // rows
                        "[]"));

                    //---- label2 ----
                    label2.setText("\u5b66\u53f7\uff1a");
                    panel3.add(label2, "cell 0 0");
                    panel3.add(textField1, "cell 1 0,wmin 100");
                }
                contentPanel.add(panel3, "cell 0 0");

                //---- button1 ----
                button1.setText("\u67e5\u627e");
                button1.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        selectMouseClicked(e);
                    }
                });
                contentPanel.add(button1, "pad 0 40 0 -20,cell 0 2,wmin 10");
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JPanel panel3;
    private JLabel label2;
    private JTextField textField1;
    private JButton button1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
