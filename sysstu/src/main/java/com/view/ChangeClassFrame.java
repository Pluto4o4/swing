/*
 * Created by JFormDesigner on Sun Oct 23 00:41:28 CST 2022
 */

package com.view;

import com.beans.Class2;
import com.dao.ChangeClassDao;
import com.dao.InsertClassDao;
import com.dao.SelectClassDao;
import com.sun.org.apache.bcel.internal.generic.Select;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author 1
 */
public class ChangeClassFrame extends JDialog {
    public ChangeClassFrame(Window owner,String id) {
        super(owner);
        initComponents();
        Class2 cls;
        cls=new SelectClassDao().outcome(id);
        this.textField3.setText(cls.getClassid());
        this.textField3.setEditable(false);
        this.textField2.setText(cls.getClassname());
    }

    private void AddClassMouseClicked(MouseEvent e) {
        // TODO add your code here
        Class2 cls=new Class2();
        cls.setClassid(this.textField3.getText());
        cls.setClassname(this.textField2.getText());
        new ChangeClassDao().change(cls);
        this.setVisible(false);
    }

    private void button3MouseClicked(MouseEvent e) {
        // TODO add your code here
        this.setVisible(false);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        panel1 = new JPanel();
        panel2 = new JPanel();
        label2 = new JLabel();
        label3 = new JLabel();
        textField2 = new JTextField();
        textField3 = new JTextField();
        button2 = new JButton();
        button3 = new JButton();

        //======== this ========
        setTitle("\u4fee\u6539");
        Container contentPane = getContentPane();
        contentPane.setLayout(new MigLayout(
            "insets 0,hidemode 3,align center center",
            // columns
            "[fill]",
            // rows
            "[fill]"));

        //======== panel1 ========
        {
            panel1.setLayout(null);

            //======== panel2 ========
            {
                panel2.setLayout(null);

                {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panel2.getComponentCount(); i++) {
                        Rectangle bounds = panel2.getComponent(i).getBounds();
                        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel2.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel2.setMinimumSize(preferredSize);
                    panel2.setPreferredSize(preferredSize);
                }
            }
            panel1.add(panel2);
            panel2.setBounds(new Rectangle(new Point(5, 5), panel2.getPreferredSize()));

            //---- label2 ----
            label2.setText("\u73ed\u7ea7id\uff1a");
            panel1.add(label2);
            label2.setBounds(new Rectangle(new Point(45, 45), label2.getPreferredSize()));

            //---- label3 ----
            label3.setText("\u73ed\u7ea7\u540d\uff1a");
            panel1.add(label3);
            label3.setBounds(new Rectangle(new Point(40, 80), label3.getPreferredSize()));
            panel1.add(textField2);
            textField2.setBounds(90, 75, 100, textField2.getPreferredSize().height);
            panel1.add(textField3);
            textField3.setBounds(90, 35, 100, textField3.getPreferredSize().height);

            //---- button2 ----
            button2.setText("\u6dfb\u52a0");
            button2.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    AddClassMouseClicked(e);
                }
            });
            panel1.add(button2);
            button2.setBounds(new Rectangle(new Point(35, 125), button2.getPreferredSize()));

            //---- button3 ----
            button3.setText("\u8fd4\u56de");
            button3.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    button3MouseClicked(e);
                }
            });
            panel1.add(button3);
            button3.setBounds(new Rectangle(new Point(140, 125), button3.getPreferredSize()));

            {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                    Rectangle bounds = panel1.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(panel1, "cell 0 0,wmin 250,hmin 170");
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JPanel panel1;
    private JPanel panel2;
    private JLabel label2;
    private JLabel label3;
    private JTextField textField2;
    private JTextField textField3;
    private JButton button2;
    private JButton button3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
