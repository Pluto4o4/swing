package com.Utility;

import com.alibaba.excel.EasyExcel;
import com.beans.Student;
import com.dao.StudentDao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExcelUtils {
    public void simpleWrite() {

        String path = "c:\\USERS\\14867\\Desktop\\excel\\";
        // 注意 simpleWrite在数据量不大的情况下可以使用（5000以内，具体也要看实际情况），数据量大参照 重复多次写入
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        String time = simpleDateFormat.format(date) +"";
        String filename = "学生名单导出";


        // 写法1 JDK8+
        // since: 3.0.0-beta1
        String fileName = path + "学生名单导出" + System.currentTimeMillis() + ".xlsx";
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        EasyExcel.write(fileName, Student.class)
                .sheet("模板")
                .doWrite(() -> {
                    // 分页查询数据

                    try {
                        return data();
                    } catch (SQLException e) {
                        System.out.println("SQLExeception");
                        throw new RuntimeException(e);
                    }

                });
    }
    private List<Student> data() throws SQLException {
        StudentDao studentDao = new StudentDao();
        ResultSet re =  studentDao.getallclass();
        List<Student> list = new ArrayList<>();

        while(re.next()){
            Student student = new Student();
            student.setSno(re.getString("sno"));
            student.setSname(re.getString("sname"));
            student.setGender(re.getString("gender"));
            student.setClassid(re.getString("classid"));
            student.setMinzu(re.getString("minzu"));
            student.setPhone(re.getString("phone"));
            student.setBirthDay(re.getString("birthday"));
            student.setAddress(re.getString("address"));
            student.setInterest(re.getString("interest"));
            student.setJianli(re.getString("jianli"));
            student.setPhotopath(re.getString("photopath"));
            list.add(student);

        }


        return list;
    }
}
