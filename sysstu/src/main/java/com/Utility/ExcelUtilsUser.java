package com.Utility;

import com.alibaba.excel.EasyExcel;
import com.beans.Class2;
import com.beans.Student;
import com.beans.User;
import com.dao.SelectallClassDao;
import com.dao.SelectallUserDao;
import com.dao.StudentDao;

import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ExcelUtilsUser {
    public void simpleWrite() {

        String path ="c:\\USERS\\14867\\Desktop\\excel\\";
        // 注意 simpleWrite在数据量不大的情况下可以使用（5000以内，具体也要看实际情况），数据量大参照 重复多次写入
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        String time = simpleDateFormat.format(date) +"";
        String filename = "用户名单导出";


        // 写法1 JDK8+
        // since: 3.0.0-beta1
        String fileName = path + "用户单导出" + System.currentTimeMillis() + ".xlsx";
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可
        EasyExcel.write(fileName, User.class)
                .sheet("模板")
                .doWrite(() -> {
                    // 分页查询数据

                    try {
                        return data();
                    } catch (SQLException e) {
                        System.out.println("SQLExeception");
                        throw new RuntimeException(e);
                    }

                });
    }
    private List<User> data() throws SQLException {
        ResultSet re =  new SelectallUserDao().getalluser();
        List<User> list = new ArrayList<>();

        while(re.next()){
            User student = new User();
            student.setId(re.getString("id"));
            student.setPsw(re.getString("psw"));
            list.add(student);

        }


        return list;
    }
}
