package com.Utility;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @program: sysstu
 * @description: JDBC工具类
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 08:43
 **/
public class JDBCUtils {
    private static Connection con;
    private static String driver;
    private static String url;
    private static String username;
    private static String password;
    static{
        Properties pro=new Properties();
        try {
            pro.load(new FileReader("src/database.properties"));
            url=pro.getProperty("url");
            username=pro.getProperty("username");
            password=pro.getProperty("password");
            driver=pro.getProperty("driverClass");
            Class.forName(driver);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConnection() {
        try {
            con= DriverManager.getConnection(url,username,password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return con;
    }

    public static void closeConnection(Connection con){
        try {
            if(con!=null){
                con.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
