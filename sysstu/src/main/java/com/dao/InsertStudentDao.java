package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Student;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 添加学生
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 22:53
 **/
public class InsertStudentDao {
    public int insertStudent(Student stu){
        String last="null";
        PreparedStatement ps=null;
        Connection con= JDBCUtils.getConnection();
        String sql="insert into classtb values (?,?,?,?,?,?,?,?,?,?,?)";
        try {
            ps=con.prepareStatement(sql);
            ps.setString(1,stu.getSno());
            ps.setString(2,stu.getSname());
            ps.setString(3,stu.getGender());
            ps.setString(4,stu.getClassid());
            ps.setString(5,stu.getMinzu());
            ps.setString(6,stu.getPhone());
            ps.setString(7,stu.getBirthDay());
            ps.setString(8,stu.getAddress());
            ps.setString(9,stu.getInterest());
            ps.setString(10,stu.getJianli());
            ps.setString(11,stu.getPhotopath());
            int i=ps.executeUpdate();
            System.out.println(i);
            if(i>0){
                JOptionPane.showMessageDialog(null,"插入成功");
            }
            return i;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"插入失败");
            throw new RuntimeException(e);
        }
    }
}
