package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Class2;
import com.beans.Student;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 修改
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-22 12:19
 **/
public class ChangeClassDao {
   public int change(Class2 cls){
       String sql="update class set classname=? where classid=?";
       PreparedStatement ps=null;
       Connection con= JDBCUtils.getConnection();
       try {
           ps=con.prepareStatement(sql);
           ps.setString(1,cls.getClassname());
           ps.setString(2,cls.getClassid());
           int i=ps.executeUpdate();
           return i;
       } catch (SQLException e) {
           JOptionPane.showMessageDialog(null,"修改失败");
           throw new RuntimeException(e);
       }
    }
}
