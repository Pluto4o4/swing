package com.dao;

import com.Utility.JDBCUtils;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 删除学生
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-22 10:13
 **/
public class RemoveUserDao {
    public int removeclass(String id){
        String sql="delete from usertb where id=?";
        PreparedStatement ps=null;
        Connection con= JDBCUtils.getConnection();
        try {
            ps=con.prepareStatement(sql);
            ps.setString(1,id);
            int i=ps.executeUpdate();
            if(i>0){
                JOptionPane.showMessageDialog(null,"删除成功!");
            }else{
                JOptionPane.showMessageDialog(null,"删除失败!");
            }
            return i;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"删除失败!");
            throw new RuntimeException(e);
        }
    }

}
