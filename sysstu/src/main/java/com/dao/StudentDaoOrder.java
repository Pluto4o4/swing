package com.dao;

import com.Utility.JDBCUtils;

import java.sql.*;

/**
 * @program: sysstu
 * @description: 学生信息查询类
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 11:37
 **/
public class StudentDaoOrder {
    public ResultSet getallclass(String classId,String order){
       String sql="select*from classtb where classid = ? order by "+order;
        Connection con=JDBCUtils.getConnection();
       PreparedStatement ps=null;
        try {
           ps=con.prepareStatement(sql);
           ps.setString(1,classId);
//           ps.setString(2,order);
           //System.out.println(ps);
//            Statement stmt=con.createStatement();
           ResultSet rs=ps.executeQuery();
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int getallrows(String classId){
        String sql="select count(*) from classtb where classid = ?";
        try {
            Connection con=JDBCUtils.getConnection();
//            Statement stmt=con.createStatement();
           PreparedStatement ps=con.prepareStatement(sql);
           ps.setString(1,classId);
            ResultSet rs=ps.executeQuery();
            rs.next();
            int row=rs.getInt("count(*)");
            JDBCUtils.closeConnection(con);
            System.out.println(row);
            return row;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
