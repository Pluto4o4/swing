package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Class2;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 查找选中的信息
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-22 10:41
 **/
public class SelectClassDao {
    //String sql="update classtb set sno=?,sname=?,gender=?,classid=?,minzu=?,phone=?,birthday=?,address=?,interest=?,jianli=?,photopath=?";
    public Class2 outcome(String id){
        Class2 stu=new Class2();
        String sql="select*from class where classid=?";
        PreparedStatement ps=null;
        Connection con= JDBCUtils.getConnection();
        try {
            ps=con.prepareStatement(sql);
            ps.setString(1,id);
            ResultSet i=ps.executeQuery();
            if(i.next()){
               stu.setClassid(i.getString("classid"));
               stu.setClassname(i.getString("classname"));
            }else{
                JOptionPane.showMessageDialog(null,"无此班级,请重新选项！");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"无此班级，重新选择!");
            throw new RuntimeException(e);
        }
        return stu;
    }
}
