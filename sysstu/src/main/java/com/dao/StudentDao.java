package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Student;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.sql.*;
import java.util.List;

/**
 * @program: sysstu
 * @description: 学生信息查询类
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 11:37
 **/
public class StudentDao {
    public ResultSet getallclass(String order){
       String sql="select*from classtb order by "+order;
        Connection con=JDBCUtils.getConnection();
        try {
//            Statement stmt=con.createStatement();
           PreparedStatement ps=con.prepareStatement(sql);
//           ps.setString(1,order);
           System.out.println(ps);
           ResultSet rs=ps.executeQuery();
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
   public ResultSet getallclass(){
      String sql="select*from classtb";
      Connection con=JDBCUtils.getConnection();
      try {
            Statement stmt=con.createStatement();
//         PreparedStatement ps=con.prepareStatement(sql);
//         ps.setString(1,order);

         ResultSet rs=stmt.executeQuery(sql);
         return rs;
      } catch (SQLException e) {
         throw new RuntimeException(e);
      }
   }

    public int getallrows(){
        String sql="select count(*) from classtb";
        try {
            Connection con=JDBCUtils.getConnection();
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            rs.next();
            int row=rs.getInt("count(*)");
            JDBCUtils.closeConnection(con);
            System.out.println(row);
            return row;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
