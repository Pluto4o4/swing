package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Student;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 查找选中的信息
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-22 10:41
 **/
public class SelectStudentDao {
    //String sql="update classtb set sno=?,sname=?,gender=?,classid=?,minzu=?,phone=?,birthday=?,address=?,interest=?,jianli=?,photopath=?";
    public Student outcome(String id){
        Student stu=new Student();
        String sql="select*from classtb where sno=?";
        PreparedStatement ps=null;
        Connection con= JDBCUtils.getConnection();
        try {
            ps=con.prepareStatement(sql);
            ps.setString(1,id);
            ResultSet i=ps.executeQuery();
            if(i.next()){
               stu.setSno(i.getString("sno"));
               stu.setSname(i.getString("sname"));
               stu.setGender(i.getString("gender"));
               stu.setClassid(i.getString("classid"));
               stu.setMinzu(i.getString("minzu"));
               stu.setPhone(i.getString("phone"));
               stu.setBirthDay(i.getString("birthday"));
               stu.setAddress(i.getString("address"));
               stu.setInterest(i.getString("interest"));
               stu.setJianli(i.getString("jianli"));
               stu.setPhotopath(i.getString("photopath"));
               return stu;
            }else{
                JOptionPane.showMessageDialog(null,"无此学生，重新选择!");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"无此学生，重新选择!");
            throw new RuntimeException(e);
        }
        return stu;
    }
}
