package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Class2;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 添加学生
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 22:53
 **/
public class InsertClassDao {
    public int insertStudent(Class2 cls){
        PreparedStatement ps=null;
        Connection con= JDBCUtils.getConnection();
        String sql="insert into class values (?,?)";
        try {
            ps=con.prepareStatement(sql);
            ps.setString(1,cls.getClassid());
            ps.setString(2,cls.getClassname());
            int i=ps.executeUpdate();
            System.out.println(i);
            if(i>0){
                JOptionPane.showMessageDialog(null,"添加成功");
            }
            return i;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"添加失败");
            throw new RuntimeException(e);
        }
    }
}
