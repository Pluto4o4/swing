package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Student;

import javax.swing.*;
import java.awt.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 修改
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-22 12:19
 **/
public class ChangeStudentDao {
   public int change(Student stu){
       String sql="update classtb set sname=?,gender=?,classid=?,minzu=?,phone=?,birthday=?,address=?,interest=?,jianli=?,photopath=? where sno=?";
       PreparedStatement ps=null;
       Connection con= JDBCUtils.getConnection();
       try {
           ps=con.prepareStatement(sql);
           ps.setString(11,stu.getSno());
           ps.setString(1,stu.getSname());
           ps.setString(2,stu.getGender());
           ps.setString(3,stu.getClassid());
           ps.setString(4,stu.getMinzu());
           ps.setString(5,stu.getPhone());
           ps.setString(6,stu.getBirthDay());
           ps.setString(7,stu.getAddress());
           ps.setString(8,stu.getInterest());
           ps.setString(9,stu.getJianli());
           ps.setString(10,"null");
           int i=ps.executeUpdate();
           return i;
       } catch (SQLException e) {
           JOptionPane.showMessageDialog(null,"修改失败");
           throw new RuntimeException(e);
       }
    }
}
