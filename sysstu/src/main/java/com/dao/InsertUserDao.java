package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Class2;
import com.beans.User;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 添加学生
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 22:53
 **/
public class InsertUserDao {
    public int insertUser(User user){
        PreparedStatement ps=null;
        Connection con= JDBCUtils.getConnection();
        String sql="insert into usertb values (?,?)";
        try {
            ps=con.prepareStatement(sql);
            ps.setString(1,user.getId());
            ps.setString(2,user.getPsw());
            int i=ps.executeUpdate();
            System.out.println(i);
            if(i>0){
                JOptionPane.showMessageDialog(null,"添加成功");
            }
            return i;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"添加失败");
            throw new RuntimeException(e);
        }
    }
}
