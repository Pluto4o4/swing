package com.dao;

import com.Utility.JDBCUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @program: sysstu
 * @description: 学生信息查询类
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-21 11:37
 **/
public class SelectallUserDao {
    public ResultSet getalluser(){
       String sql="select*from usertb";
        Connection con=JDBCUtils.getConnection();
        try {
            Statement stmt=con.createStatement();
           ResultSet rs=stmt.executeQuery(sql);
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public int getallrows(){
        String sql="select count(*) from usertb";
        try {
            Connection con=JDBCUtils.getConnection();
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            rs.next();
            int row=rs.getInt("count(*)");
            JDBCUtils.closeConnection(con);
            System.out.println(row);
            return row;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
