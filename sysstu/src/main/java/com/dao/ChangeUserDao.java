package com.dao;

import com.Utility.JDBCUtils;
import com.beans.Class2;
import com.beans.User;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @program: sysstu
 * @description: 修改
 * @author: Mr.Xu/Pluto404
 * @create: 2022-10-22 12:19
 **/
public class ChangeUserDao {
   public int change(User cls){
       String sql="update usertb set psw=? where id=?";
       PreparedStatement ps=null;
       Connection con= JDBCUtils.getConnection();
       try {
           ps=con.prepareStatement(sql);
           ps.setString(1,cls.getPsw());
           ps.setString(2,cls.getId());
           int i=ps.executeUpdate();
           return i;
       } catch (SQLException e) {
           JOptionPane.showMessageDialog(null,"修改失败");
           throw new RuntimeException(e);
       }
    }
}
