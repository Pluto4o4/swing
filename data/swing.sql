/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 8.0.28 : Database - swingtest
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`swingtest` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `swingtest`;

/*Table structure for table `classtb` */

DROP TABLE IF EXISTS `classtb`;

CREATE TABLE `classtb` (
  `sno` varchar(20) NOT NULL,
  `sname` varchar(20) DEFAULT NULL,
  `gender` varchar(4) DEFAULT NULL,
  `classid` varchar(20) DEFAULT NULL,
  `minzu` varchar(10) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `birthday` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `interest` varchar(50) DEFAULT NULL,
  `jianli` varchar(500) DEFAULT NULL,
  `photopath` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`sno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `classtb` */

insert  into `classtb`(`sno`,`sname`,`gender`,`classid`,`minzu`,`phone`,`birthday`,`address`,`interest`,`jianli`,`photopath`) values 
('123','sfasdf','男','2班','汉族','1235435','2000-10-15','sdfsdfsdf','音乐,游戏','sdfsdfsdf','null'),
('1234','2132','男','1班','汉族','12321','3213123','213123','音乐,游戏','21312312','null');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
